package net.eldiosantos.serviceloader.service.impl;

import net.eldiosantos.serviceloader.service.GreetingService;

/**
 * Created by esjunior on 03/05/2017.
 */
public class HumanGreetingService implements GreetingService {
    @Override
    public String greeet(String name) {
        return String.format("Hello, %s", name);
    }
}
