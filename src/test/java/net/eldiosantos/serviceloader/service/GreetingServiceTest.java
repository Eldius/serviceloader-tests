package net.eldiosantos.serviceloader.service;

import net.eldiosantos.serviceloader.service.impl.HumanGreetingService;
import net.eldiosantos.serviceloader.service.impl.KlingonGreetingService;
import net.eldiosantos.serviceloader.service.impl.VulcanGreetingService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.junit.Assert.*;

/**
 * Created by esjunior on 03/05/2017.
 */
public class GreetingServiceTest {
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void greeet() throws Exception {
        Class<?>[] arrayTmp = {
            HumanGreetingService.class
            , VulcanGreetingService.class
        };
        final Set<Class<?>> implementationsList = new HashSet<>(Arrays.asList(arrayTmp));
        final Set<Class<?>> loadedImplementationsList = new HashSet<>(Arrays.asList(arrayTmp));

        final ServiceLoader<GreetingService> loader = ServiceLoader.load(GreetingService.class);
        System.out.println("Starting load test...");
        //loader.forEach(gs -> System.out.println(String.format("class: %s => %s", gs.getClass(), gs.greeet("Eldius"))));

        final Stream<GreetingService> targetStream = StreamSupport.stream(
            Spliterators.spliteratorUnknownSize(loader.iterator(), Spliterator.ORDERED)
            , false
        );

        targetStream
            .peek(g -> System.out.println(String.format("class: %s => %s", g.getClass(), g.greeet("Eldius"))))
            .peek(g -> loadedImplementationsList.add(g.getClass()))
            .map(g -> g.greeet("Eldius"))
            .forEach(System.out::println);

        assertFalse("Klingon is not spoken, Klingon is killed", loadedImplementationsList.contains(KlingonGreetingService.class));
        assertTrue("Is the implementation list and the loaded list is equals?", implementationsList.equals(loadedImplementationsList));
    }

}
